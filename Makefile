## Force Build Docker Stack
force-build:
	@DOCKER_BUILDKIT=1 docker-compose build --pull

## Raise Docker Stack
up:
	@docker-compose up -d
## Destroy docker stack
down:
	@docker-compose down -v --remove-orphans
## Setup Command
setup:
	@make up

## Get Docker Service List
ps:
	@docker-compose ps
## Get Logs
logs:
	@docker-compose logs -f
## Exec in PHP Container
php-exec:
	@docker-compose exec php $(ARGS)
## Exec in PHP Container
php:
	@docker-compose exec php sh
## Exec in Nginx Container
nginx:
	@docker-compose exec nginx sh